// This is an example for a login on GMX.net
// This project is coded with the Lazarus-IDE
//
// Regards,
// Imperial
//
// Visit: CoderBay.net

unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Menus, IdHTTP, IdSSLOpenSSL, IdMultipartFormData, IdCookieManager;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    IdCookieManager1: TIdCookieManager;
    IdHTTP1: TIdHTTP;
    IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL;
    procedure Button1Click(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
var Params: TStringList;
begin
     IdHTTP1.ProtocolVersion:= TIdHTTPProtocolVersion.pv1_1; // Set HTTP Protocol Version To v1.1 (Default Value)
     IdHTTP1.HTTPOptions:= IdHTTP1.HTTPOptions + [TIdHTTPOption.hoKeepOrigProtocol]; // Very important, otherwise Indy sets the HTTP Protocol to v1.0

     // You need a vaild User-Agent, otherwise you get wrong Cookies
     IdHTTP1.Request.UserAgent:= 'Mozilla/5.0 (Linux; U; Android 2.2.1; en-us; Nexus One Build/FRG83) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1';

     // Set all parameters
     Params:= TStringList.Create;
     Params.Append('service=' + 'freemail');
     Params.Append('successURL=' + 'https://navigator.gmx.net/login');
     Params.Append('loginErrorURL=' + 'https://navigator.gmx.net/loginerror');
     Params.Append('loginFailedURL=' + 'http://www.gmx.net/logoutlounge/free_ssl/?status=login-failed&site=gmx&agof=97_L&pg=null&pa=-1&pp=___NULL&region=de');
     Params.Append('statistics=' + '');
     Params.Append('username=' + Edit1.Text + '1');
     Params.Append('password=' + Edit2.Text);
     Params.Append('uinguserid=' + '');

     try
       IdHTTP1.Get('http://www.gmx.net/'); // Get vaild Cookies

       IdHTTP1.Request.ContentType:= 'application/x-www-form-urlencoded';
       IdHTTP1.Request.ContentLength:= Length(Trim(Params.Text));

       IdHTTP1.Post('https://service.gmx.net/de/cgi/login?hal=true', Params);

       if IdHTTP1.Response.ResponseCode = 200 then
         ShowMessage('Login successful')
       else if IdHTTP1.ResponseCode = 401 then
         ShowMessage('Login invaild')
       else
         ShowMessage('Error')
     finally
       Params.Free;
     end;
     finally
     end;
end;

end.

